package com.vimage.peopleinfo;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Dimon on 20.05.2016.
 */
public class ChangeInfoActivity extends Activity {

    private static final int CAMERA_RESULT = 132;
    TextView editDate;
    EditText editName;
    EditText editMobilePhone;
    EditText editHomePhone;
    EditText editEmail;
    ImageView editPhoto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.change_info);

        editDate = (TextView) findViewById(R.id.textViewDoB);
        editName = (EditText) findViewById(R.id.textViewName);
        editMobilePhone = (EditText) findViewById(R.id.textViewPhoneMobile);
        editHomePhone = (EditText) findViewById(R.id.textViewPhoneHome);
        editEmail = (EditText) findViewById(R.id.textViewEmail);
        editPhoto = (ImageView) findViewById(R.id.imageViewPhoto);

        Bundle extras = getIntent().getExtras();
        editName.setText(extras.getString("name"));
        editHomePhone.setText(extras.getString("homePhone"));
        editMobilePhone.setText(extras.getString("mobilePhone"));
        editEmail.setText(extras.getString("email"));
        editDate.setText(extras.getString("date"));
        editPhoto.setImageBitmap(null);
        Bitmap imagePhoto = extras.getParcelable("photo");
        MainActivity.setImageBitmapFixed(this,editPhoto,imagePhoto);
        //editPhoto.setImageBitmap(imagePhoto);


    }

    public void changeDoB(View view) {
        createDatePickerDialog(view);
    }


    public void createDatePickerDialog(View view) {

        Date date = null;
        SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");  // конвертнём в дату из строки
        try {
            date = format.parse(String.valueOf(editDate.getText()));
        } catch (ParseException e) {
            e.printStackTrace();
        }


        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        int year = calendar.get(Calendar.YEAR);
        final int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        new DatePickerDialog(ChangeInfoActivity.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                editDate.setText(dayOfMonth + "." + (monthOfYear + 1) + "." + year);
            }
        }, year, month, day).
                show();


    }

    public void clickToPhoto(View view) {
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(cameraIntent, CAMERA_RESULT);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CAMERA_RESULT) {
            if (resultCode == RESULT_OK) {
                Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
                if (thumbnail != null) {
                    ImageView ivCamera = (ImageView) findViewById(R.id.imageViewPhoto);
                    MainActivity.setImageBitmapFixed(this,ivCamera,thumbnail);

                    //ivCamera.setImageBitmap(thumbnail);
                }
            }
        }
    }


    public void close(View view) {
        //String about = editTextFirstName.getText().toString() + " " + editTextLastName.getText().toString();
        String date = editDate.getText().toString();
        String name = editName.getText().toString();
        String mobilePhone = editMobilePhone.getText().toString();
        String homePhone = editHomePhone.getText().toString();
        String email = editEmail.getText().toString();

        editPhoto.buildDrawingCache();
        Bitmap imagePhoto = editPhoto.getDrawingCache();

        Intent intent = new Intent();
        intent.putExtra("date", date);
        intent.putExtra("name", name);
        intent.putExtra("mobilePhone", mobilePhone);
        intent.putExtra("homePhone", homePhone);
        intent.putExtra("email", email);
        intent.putExtra("photo", imagePhoto);


        setResult(RESULT_OK, intent);
        finish();
    }
}

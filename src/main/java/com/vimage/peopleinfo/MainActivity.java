package com.vimage.peopleinfo;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    TextView textViewEmail;
    TextView textViewMobileNumber;
    TextView textViewHomeNumber;
    TextView textViewDoB;
    TextView textViewName;
    ImageView imageViewPhoto;


    public static final int REQUEST_CODE_EDIT = 133;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textViewEmail = (TextView) findViewById(R.id.textViewEmail);
        textViewMobileNumber = (TextView) findViewById(R.id.textViewPhoneMobile);
        textViewHomeNumber = (TextView) findViewById(R.id.textViewPhoneHome);
        textViewName = (TextView) findViewById(R.id.textViewName);
        textViewDoB = (TextView) findViewById(R.id.textViewDoB);
        imageViewPhoto = (ImageView) findViewById(R.id.imageViewPhoto);
    }


    public void change(View view) {

        String date = textViewDoB.getText().toString();
        String name = textViewName.getText().toString();
        String mobilePhone = textViewMobileNumber.getText().toString();
        String homePhone = textViewHomeNumber.getText().toString();
        String email = textViewEmail.getText().toString();

        imageViewPhoto.buildDrawingCache();
        //// TODO: 21.05.2016 странно, но передаётся старое изображение, не успел разобраться...
        // UPD: Решение в setImageBitmapFixed

        Bitmap imagePhoto = imageViewPhoto.getDrawingCache();

        Intent intent = new Intent(MainActivity.this, ChangeInfoActivity.class);
        intent.putExtra("date", date);
        intent.putExtra("name", name);
        intent.putExtra("mobilePhone", mobilePhone);
        intent.putExtra("homePhone", homePhone);
        intent.putExtra("email", email);
        intent.putExtra("photo", imagePhoto);

        startActivityForResult(intent, REQUEST_CODE_EDIT);
    }

    public void sendEmail() {
        String sendText = getString(R.string.emailGreetings);
        Intent email = new Intent(Intent.ACTION_SEND);
        email.putExtra(Intent.EXTRA_EMAIL, new String[]{textViewEmail.getText().toString()});
        email.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.emailSubject));
        email.putExtra(Intent.EXTRA_TEXT, sendText);
        email.setType("message/rfc822");
        startActivity(Intent.createChooser(email, getString(R.string.emailClientChoose)));
    }

    public void emailClick(View view) {
        sendEmail();

    }


    private void callToNumber(String sNumber) {
        Intent dialIntent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" +
                sNumber));
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        startActivity(dialIntent);

    }

    public void callToHome(View view) {
        callToNumber(textViewHomeNumber.getText().toString());
    }

    public void callToMobile(View view) {
        callToNumber(textViewMobileNumber.getText().toString());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == REQUEST_CODE_EDIT) {
            if (resultCode == RESULT_OK) {
                textViewName.setText(data.getStringExtra("name"));
                textViewHomeNumber.setText(data.getStringExtra("homePhone"));
                textViewMobileNumber.setText(data.getStringExtra("mobilePhone"));
                textViewEmail.setText(data.getStringExtra("email"));
                textViewDoB.setText(data.getStringExtra("date"));

                Bitmap imagePhoto = data.getParcelableExtra("photo");
                setImageBitmapFixed(this, imageViewPhoto, imagePhoto);
                //imageViewPhoto.setImageBitmap(imagePhoto);


            }
        }
        super.onActivityResult(requestCode, resultCode, data);

    }


    public static void setImageBitmapFixed(Context context, final ImageView imageView, Bitmap bm) {
        // капец какой-то, алхимия. Кое-как добился, чтобы битмап сохранился в imageView.
        //Новое изображение показывалось, но потом не передавалось во вторую активность

        imageView.setImageDrawable(null);
        imageView.setVisibility(View.VISIBLE);
        imageView.post(new Runnable() {
            @Override
            public void run() {
                imageView.setVisibility(View.GONE);
                imageView.setVisibility(View.VISIBLE);
            }
        });
        imageView.setImageDrawable(new BitmapDrawable(context.getResources(), bm));

    }

}
